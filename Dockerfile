FROM openjdk:9-jre
VOLUME /tmp
ADD target/takehome.jar takehome.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/takehome.jar"]