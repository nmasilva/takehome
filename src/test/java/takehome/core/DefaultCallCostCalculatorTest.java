package takehome.core;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;
import java.util.Date;

import org.junit.Test;

public class DefaultCallCostCalculatorTest {

	@Test
	public void calculateTimeLess5minSuccess() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date(0));
		calendar.set(Calendar.MINUTE, 4);
		
		assertEquals(Float.valueOf((float) 0.4) ,Float.valueOf(new DefaultCallCostCalculator().calculate(calendar.getTime())));
	}
	
	@Test
	public void calculateTimeLess5min4Success() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date(0));
		calendar.set(Calendar.MINUTE, 4);
		calendar.set(Calendar.SECOND, 1);
		
		assertEquals(Float.valueOf((float) 0.5) ,Float.valueOf(new DefaultCallCostCalculator().calculate(calendar.getTime())));
	}
	
	@Test
	public void calculateTimeMore5minSuccess() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date(0));
		calendar.set(Calendar.MINUTE, 6);
		calendar.set(Calendar.SECOND, 1);
		
		assertEquals(Float.valueOf((float) 0.6) ,Float.valueOf(new DefaultCallCostCalculator().calculate(calendar.getTime())));
	}
}
