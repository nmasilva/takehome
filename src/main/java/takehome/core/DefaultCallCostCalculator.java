package takehome.core;

import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component
class DefaultCallCostCalculator implements CallCostCalculator {

	@Override
	public float calculate(Date time) {
		float totalCost = 0;
		int minutes = calculateTimeSpent(time);
		if(minutes <= 5 ) {
			totalCost = (float) (minutes * 0.1);
		} else {
			totalCost = (float)((5 * 0.1) + (minutes-5) * 0.05);
		}
		
		return totalCost;
	}

	private int calculateTimeSpent(Date callTime) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(callTime);
		int minutes = cal.get(Calendar.MINUTE);
		if(cal.get(Calendar.SECOND) != 0) {
			minutes++;
		}
		
		return minutes;
	}	
}