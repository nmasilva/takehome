package takehome.core;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class PhoneCall {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	private int callerNumber;
	private int calleeNumber;
	private Date startTimestamp;
	private Date endTimestamp;
	private boolean inbound;

	public PhoneCall() {
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getCallerNumber() {
		return callerNumber;
	}
	public void setCallerNumber(int callerNumber) {
		this.callerNumber = callerNumber;
	}
	public int getCalleeNumber() {
		return calleeNumber;
	}
	public void setCalleeNumber(int calleeNumber) {
		this.calleeNumber = calleeNumber;
	}
	public Date getStartTimestamp() {
		return startTimestamp;
	}
	public void setStartTimestamp(Date startTimestamp) {
		this.startTimestamp = startTimestamp;
	}
	public Date getEndTimestamp() {
		return endTimestamp;
	}
	public void setEndTimestamp(Date endTimestamp) {
		this.endTimestamp = endTimestamp;
	}
	public boolean isInbound() {
		return inbound;
	}
	public void setInbound(boolean inbound) {
		this.inbound = inbound;
	}
}