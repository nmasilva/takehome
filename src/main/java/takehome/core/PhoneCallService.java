package takehome.core;

import java.util.Date;
import java.util.Map;
import java.util.Optional;

public interface PhoneCallService {

	void insertPhoneCalls(Iterable<PhoneCall> phoneCalls);

	void deletePhoneCall(long phoneCallId) throws UnexistingPhoneCallException;

	PhoneCall getPhoneCall(long phoneCallId) throws UnexistingPhoneCallException;
	
	Iterable<PhoneCall> getPhoneCalls(Optional<PhoneCallType> phoneCallType, int page, int size);

	Map<Date, Statistics> getStatistics();
	
	boolean liveCheck();
}