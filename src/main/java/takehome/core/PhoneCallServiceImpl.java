package takehome.core;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
class PhoneCallServiceImpl implements PhoneCallService {
	@Autowired
	private PhoneCallDao phoneCallDao;
	
	@Autowired
	private CallCostCalculator callCostCalculator;
	
	public PhoneCallServiceImpl() {}
	
	@Transactional
	@Override
	public void insertPhoneCalls(Iterable<PhoneCall> phoneCalls) {
		for (PhoneCall phoneCall : phoneCalls) {
			phoneCallDao.save(phoneCall);
		}
	}
	
	@Transactional
	@Override
	public void deletePhoneCall(long phoneCallId) throws UnexistingPhoneCallException {
		Optional<PhoneCall> phoneCall = phoneCallDao.findById(phoneCallId);
		if(phoneCall.isPresent()) {
			phoneCallDao.delete(phoneCall.get());			
		} else {
			throw new UnexistingPhoneCallException(phoneCallId);
		}
	}
	
	@Override
	public PhoneCall getPhoneCall(long phoneCallId) throws UnexistingPhoneCallException {
		Optional<PhoneCall> phoneCall = phoneCallDao.findById(phoneCallId);
		if(phoneCall.isPresent()) {
			return phoneCall.get();
		} else {
			throw new UnexistingPhoneCallException(phoneCallId);
		}
	}
	
	@Override
	public Iterable<PhoneCall> getPhoneCalls(Optional<PhoneCallType> phoneCallType, int page, int size) {
		Iterable<PhoneCall> phoneCalls;
		Pageable pageable = PageRequest.of(page, size);
		if(phoneCallType.isPresent()) {
			phoneCalls = phoneCallDao.getAllCalls(pageable, phoneCallType.get().equals(PhoneCallType.INBOUND));
		} else {
			phoneCalls = phoneCallDao.findAll(pageable).getContent();
		}
		
		return phoneCalls;
	}

	@Override
	public Map<Date, Statistics> getStatistics() {
		Map<Date, Statistics.Builder> builders = new HashMap<>();
		
		for (DateCallTime callTime : phoneCallDao.getTotalCallDuration(true)) {
			getStatisticsBuilder(builders, callTime.getDate()).withInboundTotalCallDuration(callTime.getTime());
		}
		for (DateCallTime callTime : phoneCallDao.getTotalCallDuration(false)) {
			getStatisticsBuilder(builders, callTime.getDate()).withOutboundTotalCallDuration(callTime.getTime());
			getStatisticsBuilder(builders, callTime.getDate()).withTotalCost(callCostCalculator.calculate(callTime.getTime()));
		}
		for (DateCallCount callCount : phoneCallDao.getTotalNumberCalls()) {
			getStatisticsBuilder(builders, callCount.getDate()).withCallTotalNumber(callCount.getCount());
		}
		for (DateCallNumbers callNumbers : phoneCallDao.getCallerNumberCalls()) {
			getStatisticsBuilder(builders, callNumbers.getDate()).withCallerCallNumber(callNumbers);
		}
		for (DateCallNumbers callNumbers : phoneCallDao.getCalleeNumberCalls()) {
			getStatisticsBuilder(builders, callNumbers.getDate()).withCalleeCallNumber(callNumbers);
		}
		
		return createDailyStatistics(builders);
	}

	private Map<Date, Statistics> createDailyStatistics(Map<Date, Statistics.Builder> builders) {
		Map<Date, Statistics> dailyStatistics = new HashMap<>();
		for (Date date : builders.keySet()) {
			dailyStatistics.put(date, builders.get(date).build());
		}
		return dailyStatistics;
	}

	private Statistics.Builder getStatisticsBuilder(Map<Date, Statistics.Builder> dailyStatistics, Date date) {
		Statistics.Builder builder = dailyStatistics.get(date);
		if(builder == null) {
			builder = new Statistics.Builder();
			dailyStatistics.put(date, builder);
		}
		
		return builder;
	}

	@Override
	public boolean liveCheck() {
		return phoneCallDao.liveCheck() >= 0;
	}
}