package takehome.core;

import java.util.Date;

class DateCallTime {
	private final Date date;
	private final Date time;
	
	public DateCallTime(Date date, Date time) {
		this.date = date;
		this.time = time;
	}

	public Date getDate() {
		return date;
	}
	public Date getTime() {
		return time;
	}
}