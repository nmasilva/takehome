package takehome.core;

public class UnexistingPhoneCallException extends Exception {
	private static final long serialVersionUID = 3592430559034855451L;

	public UnexistingPhoneCallException(long phoneCallId) {
		super("Unexisting phone call: Id = " + phoneCallId);
	}
}