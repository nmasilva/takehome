package takehome.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

public class Statistics {
	private final Date inboundTotalCallDuration;
	private final Date outboundTotalCallDuration;
	private final long callTotalNumber;
	private final Iterable<DateCallNumbers> callerCallNumber;
	private final Iterable<DateCallNumbers> calleeCallNumber;
	private final float totalCost; 
	
	private Statistics(Builder builder) {
		this.inboundTotalCallDuration = builder.inboundTotalCallDuration;
		this.outboundTotalCallDuration = builder.outboundTotalCallDuration;
		this.callTotalNumber = builder.callTotalNumber;
		this.callerCallNumber = builder.callerCallNumber;
		this.calleeCallNumber = builder.calleeCallNumber;
		this.totalCost = builder.totalCost;
	}
	
	public Date getInboundTotalCallDuration() {
		return inboundTotalCallDuration;
	}
	public Date getOutboundTotalCallDuration() {
		return outboundTotalCallDuration;
	}
	public long getCallTotalNumber() {
		return callTotalNumber;
	}
	public Iterable<DateCallNumbers> getCallerCallNumber() {
		return callerCallNumber;
	}
	public Iterable<DateCallNumbers> getCalleeCallNumber() {
		return calleeCallNumber;
	}
	public float getTotalCost() {
		return totalCost;
	}

	public static class Builder {
		private Date inboundTotalCallDuration;
		private Date outboundTotalCallDuration;
		private long callTotalNumber; 
		private Collection<DateCallNumbers> callerCallNumber;
		private Collection<DateCallNumbers> calleeCallNumber;
		private float totalCost;
		
		public Builder() {
			callerCallNumber = new ArrayList<>();
			calleeCallNumber = new ArrayList<>();
		}
		
		public Statistics build() {
			return new Statistics(this);
		}
		
		public Builder withInboundTotalCallDuration(Date inboundTotalCallDuration) {
			this.inboundTotalCallDuration = inboundTotalCallDuration;
			
			return this;
		}
		
		public Builder withOutboundTotalCallDuration(Date outboundTotalCallDuration) {
			this.outboundTotalCallDuration = outboundTotalCallDuration;
			
			return this;
		}
		
		public Builder withCallTotalNumber(long callTotalNumber) {
			this.callTotalNumber = callTotalNumber;
			
			return this;
		}
		
		public Builder withCallerCallNumber(DateCallNumbers callerCallNumber) {
			this.callerCallNumber.add(callerCallNumber);
			
			return this;
		}
		
		public Builder withCalleeCallNumber(DateCallNumbers calleeCallNumber) {
			this.calleeCallNumber.add(calleeCallNumber);
			
			return this;
		}
		
		public Builder withTotalCost(float cost) {
			this.totalCost += cost;
			
			return this;
		}
	}
}