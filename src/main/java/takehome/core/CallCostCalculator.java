package takehome.core;

import java.util.Date;

interface CallCostCalculator {
	float calculate(Date time);
}