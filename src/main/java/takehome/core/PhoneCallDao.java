package takehome.core;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

interface PhoneCallDao extends PagingAndSortingRepository<PhoneCall, Long> {

	@Query("SELECT p FROM PhoneCall p WHERE p.inbound = :isInbound")
	Page<PhoneCall> getAllCalls(Pageable pageable, @Param("isInbound") boolean isInbound);
	
	@Query("SELECT new takehome.core.DateCallTime(cast(p.startTimestamp as date), cast(SUM(p.endTimestamp-p.startTimestamp) AS time)) FROM PhoneCall p WHERE p.inbound = :isInbound GROUP BY 1")
	List<DateCallTime> getTotalCallDuration(@Param("isInbound") boolean isInbound);
	
	@Query("SELECT new takehome.core.DateCallCount(cast(p.startTimestamp as date), COUNT(p.id)) FROM PhoneCall p GROUP BY 1")
	List<DateCallCount> getTotalNumberCalls();
	
	@Query("SELECT new takehome.core.DateCallNumbers(cast(p.startTimestamp as date), COUNT(p.callerNumber), p.callerNumber) FROM PhoneCall p GROUP BY 1, 3")
	List<DateCallNumbers> getCallerNumberCalls(); 
	
	@Query("SELECT new takehome.core.DateCallNumbers(cast(p.startTimestamp as date), COUNT(p.calleeNumber), p.calleeNumber) FROM PhoneCall p GROUP BY 1, 3")
	List<DateCallNumbers> getCalleeNumberCalls();

	@Query("SELECT COUNT(1) FROM PhoneCall")
	int liveCheck(); 
}