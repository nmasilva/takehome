package takehome.core;

import java.util.Date;

class DateCallCount {
	private final Date date;
	private final long count;
	
	public DateCallCount(Date date, long count) {
		this.date = date;
		this.count = count;
	}

	public Date getDate() {
		return date;
	}
	public long getCount() {
		return count;
	}
}