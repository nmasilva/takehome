package takehome.core;

import java.util.Date;

public class DateCallNumbers extends DateCallCount {
	private final int phoneNumber;
	
	public DateCallNumbers(Date date, long count, int phoneNumber) {
		super(date, count);
		this.phoneNumber = phoneNumber;
	}

	public int getPhoneNumber() {
		return phoneNumber;
	}
}