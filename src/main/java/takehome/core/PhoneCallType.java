package takehome.core;

public enum PhoneCallType {
	INBOUND,
	OUTBOUND
}