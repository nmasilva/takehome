package takehome.rest;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import takehome.core.DateCallNumbers;
import takehome.core.PhoneCallService;
import takehome.core.PhoneCallType;
import takehome.core.Statistics;
import takehome.core.UnexistingPhoneCallException;

@RestController
@RequestMapping(value = "/call")
class PhoneCallController {
	@Autowired
	private PhoneCallService phoneCallService;
	
	@PostMapping(value = "/")
	ResponseEntity<Object> insertPhoneCalls(@RequestBody List<PhoneCallDto> phoneCalls) {
		phoneCallService.insertPhoneCalls(PhoneCallDtoFactory.createPhoneCalls(phoneCalls));
		
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}
	
	@DeleteMapping("/{phoneCallId}")
	void deletePhoneCall(@PathVariable(name = "phoneCallId") long phoneCallId) {
		try {
			phoneCallService.deletePhoneCall(phoneCallId);
		} catch (UnexistingPhoneCallException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value = "/{phoneCallId}", produces = "application/json; charset=UTF-8")
	ResponseEntity<Resource<PhoneCallDto>> getPhoneCall(@PathVariable(name = "phoneCallId") String phoneCallId) {
		try {
			PhoneCallDto phoneCallDto = PhoneCallDtoFactory.createPhoneCallDto(phoneCallService.getPhoneCall(Long.valueOf(phoneCallId)));
			Resource<PhoneCallDto> res = new Resource<PhoneCallDto>(phoneCallDto, linkTo(methodOn(this.getClass()).getPhoneCall(phoneCallId)).withSelfRel());
			return ResponseEntity.ok(res);
		} catch (UnexistingPhoneCallException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value = "/", produces = "application/json; charset=UTF-8")
	ResponseEntity<Resource<BulkPhoneCallsDto>> getAllPhoneCalls(@RequestParam(required = false) String type,
			@RequestParam int page, 
			@RequestParam int size) {
		Optional<PhoneCallType> phoneCallType;
		if(type != null) {
			phoneCallType = Optional.of(PhoneCallType.valueOf(type));
		} else {
			phoneCallType = Optional.empty();
		}
		
		return ResponseEntity.ok(new Resource<>(PhoneCallDtoFactory.createPhoneCallDtos(phoneCallService.getPhoneCalls(phoneCallType, page, size)), linkTo(methodOn(this.getClass()).getAllPhoneCalls(type, page, size)).withSelfRel()));
	}
	
	@GetMapping(value = "/statistics", produces = "application/json; charset=UTF-8")
	ResponseEntity<Resource<BulkStatisticsDto>> getStatistics() {
		Map<Date, Statistics> statistics = phoneCallService.getStatistics();
		List<StatisticsDto> statisticsDtos = new ArrayList<>();
		for (Date date : statistics.keySet()) {
			StatisticsDto dto = new StatisticsDto();
			Statistics dayStatistics = statistics.get(date);
			dto.setDate(date);
			dto.setInboundTotalCallDuration(dayStatistics.getInboundTotalCallDuration());
			dto.setOutboundTotalCallDuration(dayStatistics.getOutboundTotalCallDuration());
			dto.setCallTotalNumber(dayStatistics.getCallTotalNumber());
			for (DateCallNumbers calledNumbers : dayStatistics.getCallerCallNumber()) {
				dto.addCallerCallNumber(new PhoneCallCountDto(calledNumbers.getPhoneNumber(), calledNumbers.getCount()));
			}
			for (DateCallNumbers calledNumbers : dayStatistics.getCalleeCallNumber()) {
				dto.addCalleeCallNumber(new PhoneCallCountDto(calledNumbers.getPhoneNumber(), calledNumbers.getCount()));
			}
			dto.setTotalCost(dayStatistics.getTotalCost());
			
			statisticsDtos.add(dto);
		}
		Resource<BulkStatisticsDto> resources = new Resource<>(new BulkStatisticsDto(statisticsDtos), linkTo(methodOn(this.getClass()).getStatistics()).withSelfRel());
		
		return ResponseEntity.ok(resources);
	}
}