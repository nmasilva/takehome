package takehome.rest;

import java.util.List;

public class BulkStatisticsDto {
	private final List<StatisticsDto> statistics;
	
	public BulkStatisticsDto(List<StatisticsDto> statistics) {
		this.statistics = statistics;
	}

	public List<StatisticsDto> getStatistics() {
		return statistics;
	}
}