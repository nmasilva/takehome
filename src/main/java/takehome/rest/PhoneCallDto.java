package takehome.rest;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import takehome.core.PhoneCallType;

class PhoneCallDto {
	@JsonIgnore
	private long id;
	private int callerNumber;
	private int calleeNumber;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
	private Date startTimestamp;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
	private Date endTimestamp;
	private PhoneCallType type;

	public PhoneCallDto() {
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getCallerNumber() {
		return callerNumber;
	}
	public void setCallerNumber(int callerNumber) {
		this.callerNumber = callerNumber;
	}
	public int getCalleeNumber() {
		return calleeNumber;
	}
	public void setCalleeNumber(int calleeNumber) {
		this.calleeNumber = calleeNumber;
	}
	public Date getStartTimestamp() {
		return startTimestamp;
	}
	public void setStartTimestamp(Date startTimestamp) {
		this.startTimestamp = startTimestamp;
	}
	public Date getEndTimestamp() {
		return endTimestamp;
	}
	public void setEndTimestamp(Date endTimestamp) {
		this.endTimestamp = endTimestamp;
	}
	public PhoneCallType getType() {
		return type;
	}
	public void setType(PhoneCallType type) {
		this.type = type;
	}
}