package takehome.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

class StatisticsDto {
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	private Date date;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "hh:mm:ss")
	private Date inboundTotalCallDuration;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "hh:mm:ss")
	private Date outboundTotalCallDuration;
	private long callTotalNumber; 
	private List<PhoneCallCountDto> callerCallNumbers;
	private List<PhoneCallCountDto> calleeCallNumbers;
	private float totalCost;
	
	StatisticsDto() {
		callerCallNumbers = new ArrayList<>();
		calleeCallNumbers = new ArrayList<>();
	}

	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Date getInboundTotalCallDuration() {
		return inboundTotalCallDuration;
	}
	public void setInboundTotalCallDuration(Date inboundTotalCallDuration) {
		this.inboundTotalCallDuration = inboundTotalCallDuration;
	}
	public Date getOutboundTotalCallDuration() {
		return outboundTotalCallDuration;
	}
	public void setOutboundTotalCallDuration(Date outboundTotalCallDuration) {
		this.outboundTotalCallDuration = outboundTotalCallDuration;
	}
	public long getCallTotalNumber() {
		return callTotalNumber;
	}
	public void setCallTotalNumber(long callTotalNumber) {
		this.callTotalNumber = callTotalNumber;
	}
	public List<PhoneCallCountDto> getCallerCallNumbers() {
		return callerCallNumbers;
	}
	public void addCallerCallNumber(PhoneCallCountDto callerCallNumber) {
		this.callerCallNumbers.add(callerCallNumber);
	}
	public List<PhoneCallCountDto> getCalleeCallNumbers() {
		return calleeCallNumbers;
	}
	public void addCalleeCallNumber(PhoneCallCountDto calleeCallNumber) {
		this.calleeCallNumbers.add(calleeCallNumber);
	}
	public float getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(float totalCost) {
		this.totalCost = totalCost;
	}
}