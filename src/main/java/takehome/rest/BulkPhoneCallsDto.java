package takehome.rest;

import java.util.List;

class BulkPhoneCallsDto {
	private final List<PhoneCallDto> phoneCalls;
	
	public BulkPhoneCallsDto(List<PhoneCallDto> phoneCalls) {
		this.phoneCalls = phoneCalls;
	}

	public List<PhoneCallDto> getPhoneCalls() {
		return phoneCalls;
	}
}