package takehome.rest;

class PhoneCallCountDto {
	private final int phoneNumber;
	private final long count;
	
	public PhoneCallCountDto(int phoneNumber, long count) {
		this.phoneNumber = phoneNumber;
		this.count = count;
	}

	public int getPhoneNumber() {
		return phoneNumber;
	}
	public long getCount() {
		return count;
	}
}