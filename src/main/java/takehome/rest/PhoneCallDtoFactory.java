package takehome.rest;

import java.util.ArrayList;
import java.util.List;

import takehome.core.PhoneCall;
import takehome.core.PhoneCallType;

final class PhoneCallDtoFactory {
	private PhoneCallDtoFactory() {
	}
	
	static BulkPhoneCallsDto createPhoneCallDtos(Iterable<PhoneCall> phoneCalls) {
		List<PhoneCallDto> phoneCallDtos = new ArrayList<>();
		for (PhoneCall phoneCall : phoneCalls) {
			phoneCallDtos.add(createPhoneCallDto(phoneCall));
		}
		
		return new BulkPhoneCallsDto(phoneCallDtos);
	}
	
	static PhoneCallDto createPhoneCallDto(PhoneCall phoneCall) {
		PhoneCallDto phoneCallDto = new PhoneCallDto();
		phoneCallDto.setId(phoneCall.getId());
		phoneCallDto.setCallerNumber(phoneCall.getCallerNumber());
		phoneCallDto.setCalleeNumber(phoneCall.getCalleeNumber());
		phoneCallDto.setStartTimestamp(phoneCall.getStartTimestamp());
		phoneCallDto.setEndTimestamp(phoneCall.getEndTimestamp());
		phoneCallDto.setType(phoneCall.isInbound() ? PhoneCallType.INBOUND : PhoneCallType.OUTBOUND);
		
		return phoneCallDto;
	}
	
	static List<PhoneCall> createPhoneCalls(List<PhoneCallDto> phoneCallDtos ) {
		List<PhoneCall> phoneCalls = new ArrayList<>();
		for (PhoneCallDto phoneCallDto : phoneCallDtos) {
			phoneCalls.add(createPhoneCall(phoneCallDto));
		}
		
		return phoneCalls;
	}
	
	private static PhoneCall createPhoneCall(PhoneCallDto phoneCallDto) {
		PhoneCall phoneCall = new PhoneCall();
		phoneCall.setId(phoneCallDto.getId());
		phoneCall.setCallerNumber(phoneCallDto.getCallerNumber());
		phoneCall.setCalleeNumber(phoneCallDto.getCalleeNumber());
		phoneCall.setStartTimestamp(phoneCallDto.getStartTimestamp());
		phoneCall.setEndTimestamp(phoneCallDto.getEndTimestamp());
		phoneCall.setInbound(phoneCallDto.getType().equals(PhoneCallType.INBOUND));
		
		return phoneCall;
	}
}