package takehome.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import takehome.core.PhoneCallService;

@RestController
@RequestMapping(value = "/health")
public class HealthController {

	@Autowired
	private PhoneCallService phoneCallService;
	
	@GetMapping(value = "/ready")
	ResponseEntity<Object> isReady() {
		return ResponseEntity.ok().build();
	}
	
	@GetMapping(value = "/live")
	ResponseEntity<Object> isLive() {
		phoneCallService.liveCheck();
		
		return ResponseEntity.ok().build();
	}
}