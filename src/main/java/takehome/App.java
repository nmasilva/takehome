package takehome;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class App extends SpringBootServletInitializer {
	public static void main(String[] args) {
	    if(args == null) {
	        throw new IllegalArgumentException();
	    }
	  
		SpringApplication.run(App.class, args);
	}
}